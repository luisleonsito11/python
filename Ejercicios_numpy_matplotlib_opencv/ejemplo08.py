import numpy as np
import cv2
import os
imagen= cv2.imread(os.path.dirname(__file__)+'\\spiderman.jpg')
imagen=cv2.resize(imagen,(300,300))


def filtro_gaussiano(matriz, sigma):
    # Crear el kernel gaussiano
    tamano = int(2 * sigma + 1)
    centro = tamano // 2
    x, y = np.mgrid[0:tamano, 0:tamano]
    kernel = np.exp(-((x - centro)**2 + (y - centro)**2) / (2 * sigma**2))
    kernel = kernel / np.sum(kernel)  # Normalizar el kernel para que la suma sea 1
    
    # Aplicar el filtro gaussiano utilizando la convolución
    resultado = np.zeros_like(matriz)
    for i in range(matriz.shape[2]):  # Iterar sobre los canales de color si la matriz tiene 3 dimensiones
        resultado[:, :, i] = np.convolve(matriz[:, :, i].flatten(), kernel.flatten(),
                                          mode='same').reshape(matriz.shape[:2])
    
    return resultado

# Ejemplo de uso
imagen = np.array([
    [[100, 50, 200], [150, 75, 100], [200, 100, 50]],
    [[50, 200, 100], [100, 150, 75], [75, 100, 150]],
    [[200, 100, 75], [75, 50, 200], [100, 200, 150]]
])

sigma = 1.5

imagen_filtrada = filtro_gaussiano(imagen, sigma)

# array_imagen = np.array(imagen)
# array_resultado = (255 - array_imagen)*7
# imagen_resultado = np.array(array_resultado, dtype=np.uint8)
cv2.imshow('Imagen original', imagen)
cv2.imshow('Imagen resultante', gausiano)
cv2.waitKey(0)
cv2.destroyAllWindows()

