import pygame
import sys
import os

# iniciar programa
pygame.init()

# pantalla
screen_width = 915
screen_height = 610
screen = pygame.display.set_mode((screen_width, screen_height))

# Rutas
def resource_pack(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath('.')
    return os.path.join(base_path, relative_path)

# fondo
asset_background = resource_pack('assets/images/background.png')
background = pygame.image.load(asset_background)

# icono
asset_icon = resource_pack('assets/images/icon.png')
icon = pygame.image.load(asset_icon)

# intro
asset_intro = resource_pack('assets/music/intro_theme.mp3')
pygame.mixer.music.load(asset_intro)

# player
asset_player = resource_pack('assets/images/player.png')
playerimg = pygame.image.load(asset_player)

# lazer
asset_bullet = resource_pack('assets/images/bullet.png')
bulletimg = pygame.image.load(asset_bullet)

# texto game over
asset_over_font = resource_pack('assets/font/blox--brk-.ttf')
over_font = pygame.font.Font(asset_over_font, 64)

# texto puntos
asset_level_font = resource_pack('assets/font/amandacute.ttf')
level_font = pygame.font.Font(asset_level_font, 32)

# titulo de ventana
pygame.display.set_caption("masistas invasores del espacio")

# icono de ventana
pygame.display.set_icon(icon)

# sonido de fondo
pygame.mixer.music.play(-1)

# velocidad del juego
clock = pygame.time.Clock()

from assets import level