import numpy as np
import matplotlib.pyplot as plt

T=np.array([87,50,75,90,55,54,68,85,82,80,45,58,66])#F
D=np.array([5.0,2.2,4.1,5.4,2.8,3.0,3.6,4.9,4.1,4.2,2.0,2.7,3.1]) #cm

#x*y    T*D
td=sum(T*D)

#y^2    T^2    
t2=sum(T*T)

#x^2    D^2    
d2=sum(D*D)

#sumx    sumD
sumd=sum(D)
#sumy    sumT
sumt=sum(T)
n=len(D)

b=(n*td-(sumd*sumt))/(n*d2-(sumd*sumd))
a=(sumt-b*sumd)/n
print("La ecuacion seria Y=",round(a,2)," + ",round(b,2),"x")
#GRAFICAR
plt.plot(D,T,'o')
plt.plot(D,a+b*D,'-')
plt.show()
