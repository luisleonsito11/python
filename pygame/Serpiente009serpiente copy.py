import pygame
import time
import random
# Dificultad
# Facil       ->  10
# Medio       ->  25
# Dificil     ->  40
# Muy dificil ->  60
# Imposible   ->  120
dificultad =10

# Tamaño de ventana
ANCHO = 800
ALTO = 600
# Inicializar ventana de juego
pygame.display.set_caption('Serpiente Versión 01')
ventana = pygame.display.set_mode((ANCHO,ALTO))
# Colores (R, G, B)
black =(0, 0, 0)
white =(255, 255, 255)
red =(255, 0, 0)
green =(0, 255, 0)
blue =(0, 0, 255)
# FPS Fotogramas por segundo
fps = pygame.time.Clock()

# Posición inicial de la cabeza de la serpiente
serpiente_posicion = [400, ALTO//2]
serpiente_cuerpo = [[100, 50]]

comida_aleatorio_x=random.randrange(1, (ANCHO//10))*10
comida_aleatorio_y=random.randrange(1, (ALTO//10))*10
comida_posicion = [comida_aleatorio_x, comida_aleatorio_y]
comida = True
direccion = 'DERECHO'
cambiar_a = direccion
marcador = 0

# logica principal
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP or event.key == ord('w'):
                cambiar_a = 'ARRIBA'
            if event.key == pygame.K_DOWN or event.key == ord('s'):
                cambiar_a = 'ABAJO'
            if event.key == pygame.K_LEFT or event.key == ord('a'):
                cambiar_a = 'IZQUIERDO'
            if event.key == pygame.K_RIGHT or event.key == ord('d'):
                cambiar_a = 'DERECHO'           
            if event.key == pygame.K_ESCAPE:
                pygame.event.post(pygame.event.Event(pygame.QUIT))

    # Asegurarse de que la serpiente no pueda moverse en la dirección opuesta instantáneamente
    if cambiar_a == 'ARRIBA' and direccion != 'ABAJO':
        direccion = 'ARRIBA'
    if cambiar_a == 'ABAJO' and direccion != 'ARRIBA':
        direccion = 'ABAJO'
    if cambiar_a == 'IZQUIERDO' and direccion != 'DERECHO':
        direccion = 'IZQUIERDO'
    if cambiar_a == 'DERECHO' and direccion != 'IZQUIERDO':
        direccion = 'DERECHO'

    # Moviendo la serpiente
    if direccion == 'ARRIBA':
        serpiente_posicion[1] -= 10
    if direccion == 'ABAJO':
        serpiente_posicion[1] += 10
    if direccion == 'IZQUIERDO':
        serpiente_posicion[0] -= 10
    if direccion == 'DERECHO':
        serpiente_posicion[0] += 10
   
    # GFX
    ventana.fill(blue)
    for pos in serpiente_cuerpo:
        pygame.draw.rect(ventana, green, pygame.Rect(pos[0], pos[1], 10, 10))

    # comida de serpiente
    pygame.draw.rect(ventana, white, pygame.Rect(comida_posicion[0], comida_posicion[1], 10, 10))

    # Refresco de la pantalla
    pygame.display.update()
    # velociad en FPS=fotogramas por segundo
    fps.tick(dificultad)