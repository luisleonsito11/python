import pygame

BLANCO =     (255, 255, 255)
AZUL =      (  0,   0, 255)
VERDE =     (  0, 255,   0)
ROJO =       (255,   0,   0)
COLORTEXTO = (  0,   0,  0)
(ancho, alto) = (800, 600)

running = True

def main():
    global running, ventana

    pygame.init()
    ventana = pygame.display.set_mode((ancho, alto))
    pygame.display.set_caption("TUFF")
    ventana.fill(BLANCO)
    pygame.display.update()

    while running:
        ev = pygame.event.get()

        for event in ev:

            if event.type == pygame.MOUSEBUTTONUP:
                drawCircle()
                pygame.display.update()
                print("ddd")

            if event.type == pygame.QUIT:
                running = False

def getPos():
    pos = pygame.mouse.get_pos()
    return (pos)

def drawCircle():
    pos=getPos()
    pygame.draw.circle(ventana, AZUL, pos, 20)


if __name__ == '__main__':
    main()