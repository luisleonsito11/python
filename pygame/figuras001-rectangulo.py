#image.png Importar la librería de Juegos
import pygame
# Inicia la libreía.
pygame.init()
# Crear una ventana de 1024X764
ventana=pygame.display.set_mode((800,300))
# Reloj de sistema desde pygame
reloj=pygame.time.Clock()
# velocidad de fps
velocidad=2

# ========================================
# Bucle principal
while True:
    # mover 1 fotograma en un segundo
    reloj.tick(velocidad)

    # Bucle para pedir eventos    		
    for evento in pygame.event.get():        
        # Compara si se presionó el boton X de la ventana
        if evento.type==pygame.QUIT:                
            # Salir
            quit()  
    #========================
    # LOGICA DEL PROGRAMA


    # =======================
    # PINTADO Y DIBUJADO
    # Pintar de color (R,G,B) El fondo
    ventana.fill((50,0,0))
                                        #(x,  y, ancho,alto)
    pygame.draw.rect(ventana,(255,69,0),(0,0,50,150))  
    pygame.draw.rect(ventana,(175,238,238),(0,100,150,150))   
    pygame.draw.rect(ventana,(100,0,0),(0,200,150,150))   
    
    # Actualiza la ventana, constantemente...
    pygame.display.update()